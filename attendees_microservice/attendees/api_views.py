from django.http import JsonResponse

from .models import Attendee

from common.json import ModelEncoder

from django.views.decorators.http import require_http_methods

import json

# from events.models import Conference   # remove

# from events.api_views import ConferenceListEncoder  # remove


from .models import ConferenceVO, AccountVO

class ConferenceVODetailEncoder(ModelEncoder):   # newly added
    model = ConferenceVO
    properties = ["name"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
        # "conference",
    ]

    # encoders = {
    #     "conference": ConferenceVODetailEncoder(),  # change to the new encoder
    # }


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),   # change to the new encoder
    }

    def get_extra_data(self, o):
        # Get the count of AccountVO objects with email equal to o.email
        count = AccountVO.objects.filter(email=o.email).count()
        # Return a dictionary with "has_account": True if count > 0
        if count > 0:
            return {"has_account": True,}
        # Otherwise, return a dictionary with "has_account": False
        else:
            return {"has_account": False,}


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):      # change the "conference_id" to conference_vo_id and set it to None so that we will not get error
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    if request.method == "GET":

        # attendees = Attendee.objects.all()
        # response = []
        # for attendee in attendees:
        #     response.append(
        #         {
        #             "name": attendee.name,
        #             "href": attendee.get_api_url(),
        #         }
        #     )
        # return JsonResponse({"attendees": response})

        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )

    elif request.method == "POST":  # or use else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            # THIS LINE IS ADDED
            conference = ConferenceVO.objects.get(import_href=conference_href)
            # THIS LINE CHANGES TO ConferenceVO and import_href
            # expects a "conference" key in the submitted JSON-encoded data. It should be the "href" value for a Conference object.


            content["conference"] = conference
        except ConferenceVO.DoesNotExist:     # THIS CHANGES TO ConferenceVO
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )



@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """

    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)

        # attendee = Attendee.objects.get(id=id)
        # return JsonResponse(
        #     {
        #         "email": attendee.email,
        #         "name": attendee.name,
        #         "company_name": attendee.company_name,
        #         "created": attendee.created,
        #         "conference": {
        #             "name": attendee.conference.name,
        #             "href": attendee.conference.get_api_url(),
        #         }
        #     }
        # )
        return JsonResponse(
            {"attendee": attendee},
            encoder=AttendeeDetailEncoder
        )

    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0,})

    elif request.method == "PUT":  # or use else:
        content = json.loads(request.body)
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            {"attendee": attendee},
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
