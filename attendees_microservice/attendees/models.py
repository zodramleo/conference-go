from django.db import models
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist


class ConferenceVO(models.Model):    # newly added object
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

class AccountVO(models.Model):
    email = models.EmailField()
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    is_active = models.BooleanField()
    updated = models.DateTimeField()

class Attendee(models.Model):
    """
    The Attendee model represents someone that wants to attend
    a conference
    """

    email = models.EmailField()
    name = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    conference = models.ForeignKey(
        ConferenceVO,                     # changed from "events.Conference" to ConferenceVO
        related_name="attendees",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def create_badge(self):
        try:
            self.badge
        except ObjectDoesNotExist:
            Badge.objects.create(attendee=self)

        ''' test codes in "python manage.py shell"
        from attendees.models import Attendee
        attendee = Attendee.objects.get(id=1)
        attendee.create_badge()
        print(attendee.badge)
        attendee.refresh_from_db()
        attendee.create_badge()
        print(attendee.badge)
        '''

    def get_api_url(self):
        return reverse("api_show_attendee", kwargs={"id": self.id})


class Badge(models.Model):
    """
    The Badge model represents the badge an attendee gets to
    wear at the conference.

    Badge is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    created = models.DateTimeField(auto_now_add=True)

    attendee = models.OneToOneField(
        Attendee,
        related_name="badge",
        on_delete=models.CASCADE,
        primary_key=True,
    )
