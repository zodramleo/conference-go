from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}  # encoder is for getting the "location" in events/api.views.py; same for line 44-46

    def default(self, o):  # "o" means the object

        #  print(ModelEncoder.__mro__)  # may use this one for debugging

        # print(ModelEncoder.__mro__)  for checking encoder applying sequence
        # if the object to decode is the same class as what's in the model property, then
        if isinstance(o, self.model):
            # create an empty dictionary that will hold the property names as keys and the property values as values
            d = {}

            # below is for getting "href"
            # if o has the attribute get_api_url
            if hasattr(o, "get_api_url"):
                #  then add its return value to the dictionary
                d["href"] = o.get_api_url()

            # for each name in the properties list
            for property in self.properties:
                # get the value of that property from the model instance given just the property name
                value = getattr(o, property)

                # below is for getting the "location" in events/api.views.py; same for line 23
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)

                # put it into the dictionary with that property name as the key
                d[property] = value

            # for the edge case of (1) name of the status in the api_list_presentations,
            # and (2) state abbreviation in the response for api_show_location.
            # same for line 64-65
            d.update(self.get_extra_data(o))

            # return the dictionary
            return d
        else:
            return super().default(o)

    # for the edge case of (1) name of the status in the api_list_presentations,
    # and (2) state abbreviation in the response for api_show_location.
    # same for line 54
    def get_extra_data(self, o):
        return {}








