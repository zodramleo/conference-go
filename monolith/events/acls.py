import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY




def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,  # getting one object (one photo) with diff. sizes
        "query": city + " " + state
    }
    # Make the request
    response = requests.get(url, headers=headers, params=params)

    ''' OR
    url = f"https://api.pexels.com/v1/search?query={city}+{state}&per_page=1&page=1"
    response = requests.get(url, headers=headers)
    '''

    # Parse the JSON response
    data = json.loads(response.content)
    # # Return a dictionary that contains a `picture_url` key and
    # #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": data["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    us_iso_code = "ISO 3166-2:US"
    # Create the URL for the geocoding API with the city and state
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{us_iso_code}US&limit=1&appid={OPEN_WEATHER_API_KEY}"

    # Make the request
    geo_response = requests.get(geo_url)
    # Parse the JSON response
    geo_data = json.loads(geo_response.content)
    # Get the latitude and longitude from the response
    lat = geo_data[0]["lat"]
    lon = geo_data[0]["lon"]
    # Create the URL for the current weather API with the latitude
    #   and longitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    # Make the request
    weather_response = requests.get(weather_url)
    # Parse the JSON response
    weather_data = json.loads(weather_response.content)
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary

    try:
        return {
            "temp": weather_data["main"]["temp"],
            "description": weather_data["weather"][0]["description"],
        }
    except:
        return {
            "temp": None,
            "description": None,
        }
