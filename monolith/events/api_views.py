from django.http import JsonResponse

from .models import Conference, Location

from common.json import ModelEncoder

from django.views.decorators.http import require_http_methods

import json

from .models import State

from .acls import get_photo, get_weather_data

# from .acls import get_photo, get_weather_data

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",

    ]


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        # "starts"
        # "ends"
        # "description"
        # "created"
        # "updated"
        # "max_presentations"
        # "max_attendees",
        "location",
    ]

    encoders = {
        "location": LocationListEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """
    if request.method == "GET":
        conferences = Conference.objects.all()
        # response = []
        # for conference in conferences:
        #     response.append(
        #         {
        #             "name": conference.name,
        #             "href": conference.get_api_url(),
        #         }
        #     )
        # return JsonResponse({"conferences": response})


        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )

    else:  # for POST
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",

    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",

    ]
    encoders = {
        "location": LocationDetailEncoder(),
    }

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        # return JsonResponse(
        #     {
        #         "name": conference.name,
        #         "starts": conference.starts,
        #         "ends": conference.ends,
        #         "description": conference.description,
        #         "created": conference.created,
        #         "updated": conference.updated,
        #         "max_presentations": conference.max_presentations,
        #         "max_attendees": conference.max_attendees,
        #         "location": {
        #             "name": conference.location.name,
        #             "href": conference.location.get_api_url(),
        #         },
        #     }
        # )

        # Use the city and state abbreviation of the Conference's Location
        # to call the get_weather_data ACL function and get back a dictionary
        # that contains the weather data

        weather_data = get_weather_data(
            conference.location.city,  # conference is pulling out as object
            conference.location.state.abbreviation,
        )
        ''' alternative solution: (1)making it to be dic, (2) call via dic
        conference_dic = {
            "conference": conference,
        }
        weather_data = get_weather_data(
            conference_dic["conference"].location.city,
            conference_dic["conference"].location.state.abbreviation,
        )
        '''
        # Include the weather data in the JsonResponse (see it in the dictionary?)

        return JsonResponse(
            {"conference": conference, "weather": weather_data,},
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        # Location.objects.filter(id=id).delete() -> would return a tuple - e.g. (3, {'example.name': "", ...,})
        # use "_" for space holder, or can assign another variable for the tuple
        # use filter, instead of get. Otherwise, would get error if not exist.
        return JsonResponse({"deleted": count > 0,})

    elif request.method == "PUT":  # or use else:
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # new code
        Conference.objects.filter(id=id).update(**content)

        # copied from get detail
        location = Conference.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """

    # response = []
    # for location in locations:
    #     response.append(
    #         {
    #             "name": location.name,
    #             "href": location.get_api_url(),
    #         }
    #     )
    # return JsonResponse({"locations": response})
    if request.method=="GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)  # use json.loads to convert the JSON-formatted string in request.body

        # report error for State instance since it is foreign key, not a string (JSON)
        # solution:(1) Look up the state based on abbreviation in the JSON, or
        # (2) Send an id value that specifically identifies the State

        # For sol(1), Get the State object and put it in the content dict
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:  # preventing someone send an invalid state like "ZZ"
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,  # 400 == Bad Request
            )

        # Use the city and state's abbreviation in the content dictionary
        # to call the get_photo ACL function
        photo = get_photo(content["city"], content["state"].abbreviation)
        # Use the returned dictionary to update the content dictionary
        content.update(photo)

        location = Location.objects.create(**content)
        return JsonResponse(  # let the sys. know if we sucessfully create.
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )





@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
    if request.method=="GET":
        location = Location.objects.get(id=id)
        # return JsonResponse(
        #     {
        #         "name": location.name,
        #         "city": location.city,
        #         "room_count": location.room_count,
        #         "created": location.created,
        #         "updated": location.updated,
        #         "state": location.state.abbreviation,
        #     }
        # )
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        # Location.objects.filter(id=id).delete() -> would return a tuple - e.g. (3, {'example.name': "", ...,})
        # use "_" for space holder, or can assign another variable for the tuple
        # use filter, instead of get. Otherwise, would get error if not exist.
        return JsonResponse({"deleted": count > 0,})

    elif request.method == "PUT":  # or use else:
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # new code
        Location.objects.filter(id=id).update(**content)

        # copied from get detail
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
