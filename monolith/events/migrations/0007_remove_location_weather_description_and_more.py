# Generated by Django 4.0.3 on 2023-02-10 06:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0006_remove_conference_weather_description_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='location',
            name='weather_description',
        ),
        migrations.RemoveField(
            model_name='location',
            name='weather_temp',
        ),
    ]
